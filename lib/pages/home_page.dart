import 'package:flutter/material.dart';

import '../blocs/movies_bloc.dart';
import '../models/item_model.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bloc.fetchAllMovies();
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Search',
                    style: TextStyle(fontSize: 30, color: Colors.white),
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  TextField(
                    style: TextStyle(fontSize: 20, color: Colors.white),
                    decoration: InputDecoration(
                      hintText: "Movies, Actors, Directors,...",
                      hintStyle: TextStyle(fontSize: 20.0, color: Colors.white),
                      border: UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.teal,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Popular',
                        style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'SEE ALL',
                        style: TextStyle(
                            fontSize: 14, color: Colors.white.withOpacity(0.5)),
                      ),
                    ],
                  ),
                  Container(
                    width: double.maxFinite,
                    height: 278,
                    // color: Colors.amber,
                    child: StreamBuilder(
                        stream: bloc.allMovies,
                        builder: (context, AsyncSnapshot<ItemModel> snapshot) {
                          if (snapshot.hasData) {
                            return Container(
                              height: 200,
                              // color: Colors.red,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: snapshot.data!.results.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    margin: EdgeInsets.only(
                                        left: 14, top: 10, bottom: 10),
                                    width: 150,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Image.network(
                                          snapshot
                                              .data!.results[index].posterPath,
                                          fit: BoxFit.fill,
                                        ),
                                        Text(
                                          snapshot.data!.results[index].title,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 14),
                                        )
                                      ],
                                    ),
                                  );
                                },
                              ),
                            );

                            return Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                    height: 200,
                                    child: FittedBox(
                                      child: Image.network(
                                        snapshot.data!.results[0].posterPath,
                                      ),
                                      fit: BoxFit.fill,
                                    )),
                              ],
                            );
                          } else if (snapshot.hasError) {
                            print('Something went wrong');
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            print('Wating..');
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                        }),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
