import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/item_model.dart';

class MovieApiProvider {
  final _apiKey = "5e950098c27958df9319fdc45adeb874";
  final _baseUrl = "https://api.themoviedb.org/3/movie";

  Future<ItemModel> fetchMovieList() async {
    print('entered');
    final response = await http.get(Uri.parse(
        "https://api.themoviedb.org/3/movie/popular?api_key=$_apiKey"));
    // print(response.body.toString());

    if (response.statusCode == 200) {
      return ItemModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load post');
    }
  }
}
