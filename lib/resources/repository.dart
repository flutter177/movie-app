import 'package:movie_app/models/item_model.dart';
import 'package:movie_app/resources/movie_api_provider.dart';

class Repository {
  final movieappprovider = MovieApiProvider();
  Future<ItemModel> fetAllMovies() => movieappprovider.fetchMovieList();
}
