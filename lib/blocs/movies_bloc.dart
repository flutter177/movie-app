import 'package:flutter/foundation.dart';
import 'package:movie_app/models/item_model.dart';
import 'package:movie_app/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class MoviesBloc {
  final respository = Repository();
  final movieFetcher = PublishSubject<ItemModel>();

  Stream<ItemModel> get allMovies => movieFetcher.stream;

  fetchAllMovies() async {
    ItemModel itemModel = await respository.fetAllMovies();
    movieFetcher.sink.add(itemModel);
  }

  dispose() {
    movieFetcher.close();
  }
}

final bloc = MoviesBloc();
